# COD Scripts

Autodesk Fabrication scripts for use in CADmep, ESTmep and CAMduct.

## Additional Resources
More great scripts can be found on Darren Young's blog.
https://www.darrenjyoung.com/resources/fabrication-cod-scripting/script-library/